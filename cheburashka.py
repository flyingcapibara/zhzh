from collections import deque

def find(adjl):        #поиск выхода
    dist = [-1 for i in range(len(adjl))]  #расстояния от комнаты до начала пути
    all_way = []            #путь к выходу
    to_visit = deque()      #комнаты к посещению
    to_visit.append(0)      #начальная комната
    dist[0] = 0             #расстояния
    answer = False       #найден ли выход
    while not answer:  
        st_room = to_visit.popleft() #посещаемая комната
        for room in adjl[st_room]:   #заходим в соседние комнаты
            if room != -1:           
                if dist[room] == -1:  #непроверенная комната
                    dist[room] = dist[st_room] + 1
                    to_visit.append(room)
            else:  #выход найден
                answer = True
                fin_room = st_room
                d = dist[fin_room]
    while fin_room != 0:
        all_way.append(fin_room)
        d -= 1
        for room in adjl[fin_room]:
            if dist[room] == d:
                new_room = room
        fin_room = new_room
    return all_way                     #путь от выхода к началу


adjl = []   #список смежности
for i in range(int(input())):
    adjl.append(list(map(int, input().split())))
print(find(adjl))
